package repres;

import api.models.UserNameRegister;
import api.steps.PostRegister;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

public class TestPostRegister {

    private final PostRegister postRegister = new PostRegister();

    @Test
    public void successRegisterUser() {
        UserNameRegister payload = new UserNameRegister("eve.holt@reqres.in","pistol");

        Response response = postRegister.postUsersSuccess(payload);
        postRegister.checkUser(response, payload);
    }

}
