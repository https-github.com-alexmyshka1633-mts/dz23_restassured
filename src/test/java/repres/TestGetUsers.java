package repres;

import api.models.GetUserString;
import api.steps.GetReqres;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class TestGetUsers {

    private final static String URL = "https://reqres.in/api/";

    private final GetReqres getReqres = new GetReqres();
    @ParameterizedTest
    @ValueSource(strings = {"1", "2"})
    public void successCheckEMail(String page) {
        GetUserString response = getReqres.getUsersSuccess2(page);

        getReqres.checkEMail(response);
    }
}
