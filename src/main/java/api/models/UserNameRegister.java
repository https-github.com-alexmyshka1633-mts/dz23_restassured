package api.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class UserNameRegister {
   private String email;
   private String password;
   private Integer id;
   private String token;

   public UserNameRegister(String email, String pistol) {

   }
}
