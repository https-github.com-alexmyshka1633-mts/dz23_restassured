package api.steps;

import api.models.PutNameUser;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class DeleteUser {
    public static Response deleteUsersSuccess() {

        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .delete("users/2")
                .then()
                .spec(SpecHelper.getResponseSpec(204))
                .extract()
                .response();
    }
}
