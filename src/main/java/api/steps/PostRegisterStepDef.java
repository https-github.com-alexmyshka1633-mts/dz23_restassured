package api.steps;

import api.models.CreateUserPayload;
import api.models.UserNameRegister;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

public class PostRegister {

    public static Response postUsersSuccess(UserNameRegister payload) {

        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .post("register")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract()
                .response();
    }
    public void checkUser(Response response, UserNameRegister payloads) {
        Assertions.assertEquals(payloads.getId(), response.jsonPath().get("id"));
        Assertions.assertEquals(payloads.getToken(), response.jsonPath().get("token"));
    }

}
