package api.steps;

import api.models.PutNameUser;
import api.models.PutNameUserCheck;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

public class PutUsers {

    public static Response putUsersSuccess(PutNameUser payload) {

        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .put("users/2")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract()
                .response();
    }

    public void checkEMail(PutNameUserCheck response) {
        Assertions.assertTrue(response.getData().stream().allMatch(x -> x.getEMail().endsWith("@reqres.in")));
    }


}
