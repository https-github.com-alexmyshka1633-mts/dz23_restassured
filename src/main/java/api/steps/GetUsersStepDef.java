package api.steps;

import api.models.GetUserString;
import api.models.ListUsersResponse;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

public class GetReqres {

    public static final String USERS = "users";

    public  GetUserString getUsersSuccess2(String page) {

        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .get(String.format(USERS + "?page=%s", page) )
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract().body().jsonPath().getObject(".", GetUserString.class);
    }

    public void checkEMail(GetUserString response) {
        Assertions.assertTrue(response.getData().stream().allMatch(x -> x.getEMail().endsWith("@reqres.in")));
    }
}
